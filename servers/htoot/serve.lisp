(in-package :cl-user)

(uiop:with-current-directory (*load-truename*)
  (load "../../build"))

(ql:quickload :cl-site-htoot)

;;
;; Start aserve, publish site directory & pages, set default package.
;;
(cl-site-htoot:start-server)
(cl-site-htoot:publish-cl-site)
(setq *package* (find-package :cl-site))

(format t "

This message from cl-site/servers/htoot/serve.lisp should report the url & port 
that the user may visit to browse the site.

")

