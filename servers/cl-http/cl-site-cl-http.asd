;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-user)

(asdf:defsystem cl-site-cl-http
  :name "cl-site-cl-http"
  :version "0.0.1"
  :maintainer "Common-Lisp.Net maintainers"
  :author "Dave Cooper & Common-Lisp.Net maintainers"
  :licence "TBD"
  :description "Test server for common-lisp.net for cl-http"
  :depends-on (:cl-site) ;; :cl-http ;; FLAG -- add cl-http when/if available in Quicklisp.
  :serial t
  :components ((:file "package")
	       (:file "publish")))

