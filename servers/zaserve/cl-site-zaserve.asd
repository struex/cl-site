;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-user)

(asdf:defsystem cl-site-zaserve
  :name "cl-site-zaserve"
  :version "0.0.1"
  :maintainer "Common-Lisp.Net maintainers"
  :author "Dave Cooper & Common-Lisp.Net maintainers"
  :licence "TBD"
  :description "Test server for common-lisp.net for :zaserve AllegroServe port"
  :depends-on (:cl-site :aserve #+allegro :bordeaux-threads) ;; FLAG -- change to :zaserve when available in QL.
  :serial t
  :components ((:file "package")
	       (:file "publish")))

