;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-site)


(defun check-deps-fn (dependencies)
   #'(lambda (processed-pages)
        (every (lambda (d)
                  (member d processed-pages :test #'string=))
               dependencies)))

(defun make-site (&optional (output-dir *output-dir*))
  (let ((pages (populate-pages)))
    (ensure-directories-exist output-dir)
    (format t "Generating site in ~A.~%" output-dir)
    (process-static *static-dir* output-dir)
    (process-static (merge-pathnames #P"static/" *layout-dir*) output-dir)

    (loop with processed-pages = '()
          with delayed-pages = '()
          for page in pages
          for page-path = (make-path *pages-dir* page t)
          for rel-page-path = (enough-namestring page-path *pages-dir*)
          for page-lines = (file-to-list page-path)
          for header = (yaml:parse (format nil "~{~A~%~}" (header-lines page-lines)))
          for content = (format nil "~{~A~%~}" (content-lines page-lines))
          ;; order in the context is very important: for any key appearing twice
          ;;  (e.g. the 'title' key), the first will be selected; by taking the header values
          ;;  first, the default 'title' being the content filename, can be overridden in the page's metadata
          for context = (append (when header (alist-from-hash header)) page)
          for dependencies = (cdr (assoc "depends-on" context :test #'string=))
          for satisfies-deps-fn = (check-deps-fn dependencies)
          for satisfies-deps-p = (funcall satisfies-deps-fn processed-pages)
      do (push (list satisfies-deps-fn content context rel-page-path) delayed-pages)
      do (loop for resolved-deps-pages =
                   (loop for delay in delayed-pages
                         for satisfies-deps-p = (funcall (car delay) processed-pages)
                         when satisfies-deps-p
                         collect delay
                         and do (progn
                                   (format t "Rendering ~a~%" (fourth delay))
                                   (push (fourth delay) processed-pages)
                                   (render-page-with-hooks (second delay) (third delay))))
               while resolved-deps-pages
               do (setf delayed-pages
                        (set-difference delayed-pages resolved-deps-pages :key #'fourth :test #'string=))))))


(defun make-clean (&optional (output-dir *output-dir*))
  (when (probe-file output-dir)
    (uiop:delete-directory-tree
     output-dir
     :validate #'(lambda(dir)
		   (string-equal (first (last (pathname-directory dir)))
				 "output"))))
  (ensure-directories-exist output-dir))
